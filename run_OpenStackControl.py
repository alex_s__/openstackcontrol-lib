__author__ = 'lex'

# Distributed under http://www.gnu.org/licenses/gpl-3.0.html
# This program run_OpenStackControl.py utilizes library lib_OpenStackControl.py
# for start\shutdown of OpenStack virtual machines with eBet Booxware services
# version 0.1

# (c) Aliaksei Shautsou , epam.lex at gmail.com


import argparse
import lib_OpenStackControl
import threading
import sys
from simpleconfigparser import simpleconfigparser
from os.path import expanduser
import subprocess
import os

def read_config():
    config = simpleconfigparser()
    home = expanduser('~')
    config.read(home+"/.config/openstackcontrol.ini")
    return config

def read_instances():
    config = simpleconfigparser()
    home = expanduser('~')
    config.read("instances.ini")
    return config

configfile = read_config()
instancefile = read_instances()

try:
    ob2 = lib_OpenStackControl.OpenStackControl_keystone(openstack_login=configfile.app.openstack_login, openstack_password=configfile.app.openstack_password, privatesshkey_pathstring=configfile.app.privatesshkey_pathstring, DEBUG=configfile.app.DEBUG,instancefile=instancefile)
except:
    print(sys.exc_info())
    sys.exit(1)

parser = argparse.ArgumentParser(description="Graceful shutdown and start of Booxware services inside Open Stack virtual machine")
group_lrs = parser.add_mutually_exclusive_group()
group_lrs.add_argument("-l", '--list_tenants',  help="list tenants", action="store_true")
group_lrs.add_argument("-w", "--list_instances",  help="list instances. Should be used together with -t", action="store_true")
group_lrs.add_argument("-r", "--run",  help="start virtual machine or all machines inside tennant or inside computenode and executes run.sh", action="store_true")
group_lrs.add_argument("-s", "--shutdown",  help="executes shutdown.sh and stops VM, or all VM's inside tennant or all VM's inside computenode. Could be used with -t or -c or -i", action="store_true")

group_tci = parser.add_mutually_exclusive_group()
group_tci.add_argument("-t", help="the tennant name", metavar='tennant_name')
group_tci.add_argument("-c",  help="the computenode", metavar='computenode_name')
group_tci.add_argument("-i", help="the VM instance", metavar='vm_name')
args = parser.parse_args()



if args.list_tenants:
    for x in ob2.get_tenants():
        print(x.name)
elif args.list_instances:
    machines = ob2.get_machines_in_tenant(args.t)
    for machine in machines:
        print(machine.name, machine.status, machine.networks)

elif args.run:
    if args.i is not None:
        ob2.start_vm(ob2.get_serverobject_by_name(servername=args.i))
    if args.t is not None:
       for serv in ob2.get_machines_in_tenant(args.t):

            threads = []

            t = threading.Thread(target=ob2.start_vm, args=(serv,))
            threads.append(t)
            t.start()


    if args.c is not None:
        pass
elif args.shutdown:
    if args.i is not None:
        ob2.shutdown_vm(ob2.get_serverobject_by_name(servername=args.i))
    if args.t is not None:
        for serv in ob2.get_machines_in_tenant(args.t):


            threads = []

            t = threading.Thread(target=ob2.shutdown_vm, args=(serv,))
            threads.append(t)
            t.start()

    if args.c is not None:

        read_config()