__author__ = 'lex'

# Distributed under http://www.gnu.org/licenses/gpl-3.0.html
#
# This is library lib_OpenStackControl.py is wrapper around novaClient and keystone.
# Main purpose is to manage OpenStack virtual machines in a flexible and useful way.
# This library extends standart functionality and adds special feautures like graceful shutdown\start for Booxware eBet project
#
#
# version 0.1
# (c) Aliaksei Shautsou , epam.lex at gmail.com

from keystoneclient.auth.identity import v2
from keystoneclient import session as keystoneSession
from keystoneclient import client as keystoneClient
from novaclient.client import Client as novaClient
import argparse
import paramiko
import time

class OpenStackControl_keystone():
    """
    https://wiki.openstack.org/wiki/OpenStackClients
    implementation with bindings from keystone and nova
    """
    def __init__(self, openstack_login, openstack_password, privatesshkey_pathstring, DEBUG, instancefile):
        self.DEBUG = int(DEBUG)
        if self.DEBUG == 1 : print('DEBUG MODE IS ON. Extended verbosity.')
        self.instacefile = instancefile
        self.privatesshkey_pathstring = privatesshkey_pathstring
        self.SessionStorage = {}
        self.NovaObjectStorage = {}
        self.ks = keystoneClient.Client(version=2,session=self.auth_method(t_name='Chef-Devel', username=openstack_login, password=openstack_password), auth_url='http://10.2.6.70:5000/v2.0')
        self.auth_nova_for_each_tenant(username=openstack_login, password=openstack_password)



    def auth_method(self, t_name, username, password):

        self.auth_object = v2.Password(auth_url='http://10.2.6.70:5000/v2.0',
                               username=username,
                               password=password,
                               tenant_name=t_name)
        return keystoneSession.Session(auth=self.auth_object)

    def auth_nova_for_each_tenant(self, username, password):
        """
        NovaClient object have to be intitizlized with specified tenant, One NovaClient object can't manage multiple tenants (23-07-2015), so each tenant require separate NovaClient object.
        :return:
        """
        for tenant in self.get_tenants():
            #print(tenant.name)
            self.SessionStorage[tenant.name] = self.auth_method(t_name=tenant.name, username=username, password=password)
            #print(self.SessionStorage)
            self.NovaObjectStorage[tenant.name] = novaClient(version=2,session=self.SessionStorage[tenant.name], auth_url='http://10.2.6.70:5000/v2.0')

    def get_tenants(self):
        return self.ks.tenants.list()

    def get_machines_in_tenant(self, t_name):
        return self.NovaObjectStorage[t_name].servers.list()

    def get_serverobject_by_name(self, tenant_name, servername):
        for server in self.NovaObjectStorage[tenant_name].servers.list():
            if server.name == servername:
                print('found ', server.name)
            else:
                print('not found ', servername)

    def get_serverobject_by_name(self, servername):

        for key, value in self.NovaObjectStorage.items():
            self.printd('looking ' + servername + ' in ' + str(key))
            if key not in ('services', 'QA-Selenium'):
                for server in value.servers.list():
                    if server.name == servername:
                        print('found ', server.name)
                        return server

    def get_ip(self, serverobj):

        for value in list(serverobj.networks.values())[0]:   # iter on both keys and values
            if value.startswith('10'):
                    return value

    def shutdown_vm(self, server_obj):
        #todo: shutdown for nginx, mysql, risk etc
        if server_obj.status == 'SHUTOFF':
            print('server ', server_obj.name, ' is already SHUTOFF')
        else:
            for section in self.instacefile.sections():
                print(section, self.instacefile[section].stop)
                self.execute_ssh_ssh_on_vm(server_obj, self.instacefile[section].stop)
            self.NovaObjectStorage['Chef-Devel'].servers.stop(server_obj)


    def start_vm(self, server_obj):
        #todo: start for nginx, mysql, risk, hazelcast , etc
        self.NovaObjectStorage['Chef-Devel'].servers.start(server_obj)
        time.sleep(90)
        for section in self.instacefile.sections():
            print(section, self.instacefile[section].run)
            self.execute_ssh_ssh_on_vm(server_obj, self.instacefile[section].run)



    def execute_ssh_ssh_on_vm(self, server_obj, cmd):
        host   = self.get_ip(server_obj)
        user   = 'booxware'
        #passwd = 'secret'
        ssh    = paramiko.SSHClient()

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, username=user,  key_filename=self.privatesshkey_pathstring)
        stdin, stdout, stderr = ssh.exec_command(cmd)
        #stdin.write(passwd + '\n')
        stdin.flush()

        stdout_list = stdout.readlines()
        stderr_list = stderr.readlines()

        self.printd('length of stdout ' + str(len(stdout_list)) + ' lines. lenght of stderr ' + str(len(stderr_list)))

        for idx, x in enumerate(stdout_list):
#        for idx, (x,y) in enumerate(zip(stdout_list,stderr_list)):
            self.printd('printinng stdout number ' + str(idx + 1) + ' of ' + str(len(stdout_list)) + ' :')
            print(x.rstrip('\n'))
#            print('printinng stderr number ' + str(idx) + ' of ' + str(len(stderr_list)) + ' :' + y)



        ssh.close()

    def get_compute_nodes(self, t_name):
        #TODO: implement https://ask.openstack.org/en/question/4373/list-compute-nodes/
        #or aggregates print('hey', self.NovaObjectStorage[t_name].aggregates.list()[1].to_dict())
        #availability zones
        #hypervisors - NO

        """
        retrieving list of compute nodes from aggregates
        http://docs.openstack.org/developer/python-novaclient/ref/v2/aggregates.html
        :param t_name:
        :return:
        """
        #print('testing first element in dict', self.NovaObjectStorage[t_name].aggregates.list()[1].to_dict())
        #print(type(self.NovaObjectStorage[t_name].aggregates.list()))
        #print(len(self.NovaObjectStorage[t_name].aggregates.list()))
        l = self.NovaObjectStorage[t_name].aggregates.list()
        print('dir of  aggregate object' ,dir(l[0]))
        print("list of aggregates")

        for x in range(len(l)):
            print(self.NovaObjectStorage[t_name].aggregates.list()[x].to_dict())

        """
        retrieving list of compute nodes from hosts
        http://docs.openstack.org/developer/python-novaclient/ref/v2/hosts.html
        """
        l = self.NovaObjectStorage[t_name].hosts.list_all()
        print('dir of host object' ,dir(l[0]))
        print("list of hosts")
        for x in l:
            print(x.to_dict())

        """
        availability zones
        it is possible to get .hosts() from zone object
        """
        l = self.NovaObjectStorage[t_name].availability_zones.list()
        print('number of avail zones = ', len(l))
        print('dir of availability zone object ',dir(self.NovaObjectStorage[t_name].availability_zones.list()[0]))
        print('list of zones')
        for x in l:
            print(x.hosts)

        """
        hypervisors
        hypervisor object could provide number of running_vm's on it, but won't return vm's\servers object
        """
        l = self.NovaObjectStorage[t_name].hypervisors.list()
        print('number of hypervisors ', len(l))
        print('dir of hypervisor object ', dir(self.NovaObjectStorage[t_name].hypervisors.list()[5]))
        print('dict of hypervisor:',self.NovaObjectStorage[t_name].hypervisors.list()[5].to_dict())

    def printd(self, debugoutput):
        if self.DEBUG == 1:
            print('DEBUG: ' + debugoutput)



#ob2 = OpenStackControl_keystone()
#print(ob2.get_tenants())
#print(ob2.get_machines_in_tenant('Casino Red (QA_D)'))
#print(ob2.get_compute_nodes('QA_B'))

#someserver = ob2.get_serverobject_by_name('w02-black')
#print(dir(someserver))
#print(someserver.status)
#print('ip of ', ob2.get_ip(someserver))
#ob2.execute_ssh_ssh_on_vm()
